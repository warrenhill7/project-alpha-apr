from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskModelForm

# Create your views here.


@login_required
def create_task_view(request):
    if request.method == "POST":
        form = TaskModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskModelForm()
    return render(request, "tasks/create_view.html", {"form": form})


@login_required
def show_task_view(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/my_tasks.html", context)


def show_my_tasks(request):
    task = Task.objects.all()
    context = {
        "task": task,
    }
    return render(request, "tasks/my_tasks.html", context)


# @login_required
# def create_project(request):
#     if request.method == "POST":
#         form = TaskModelForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("create_task")
#     else:

#         form = TaskModelForm()

#     # Put the form in the context
#     context = {
#         "form": form,
#     }
#     # Render the HTML template with the form
#     return render(request, "tasks/create.html", context)
