from django.urls import path
from tasks.views import create_task_view, show_task_view


urlpatterns = [
    path("create/", create_task_view, name="create_task"),
    path("mine/", show_task_view, name="show_my_tasks"),
]
